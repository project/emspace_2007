<?php

/**
 * Em Space 2007 admin theme.
 * Many phptemplate variable idea taken from Zen http://drupal.org/project/zen
 */

/**
 * Intercept template variables
 *
 * @param $hook
 *   The name of the theme function being executed
 * @param $vars
 *   A sequential array of variables passed to the theme function.
 */

function _phptemplate_variables($hook, $vars = array()) {

  switch ($hook) {
    // Send a new variable, $logged_in, to page.tpl.php to tell us if the current user is logged in or out.
    case 'page':
      // get the currently logged in user
      global $user;

      // An anonymous user has a user id of zero.
      if ($user->uid > 0) {
        // The user is logged in.
        $vars['logged_in'] = TRUE;
      }
      else {
        // The user has logged out.
        $vars['logged_in'] = FALSE;
      }

      $body_classes = array();
      // classes for body element
      // allows advanced theming based on context (home page, node of certain type, etc.)
      $body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';
      $body_classes[] = ($vars['logged_in']) ? 'logged-in' : 'not-logged-in';
      switch (TRUE) {
      	case $vars['sidebar_left'] && $vars['sidebar_right'] :
      		$body_classes[] = 'both-sidebars';
      		break;
      	case $vars['sidebar_left'] :
      		$body_classes[] = 'sidebar-left';
      		break;
      	case $vars['sidebar_right'] :
      		$body_classes[] = 'sidebar-right';
      		break;
      }

      global $theme_key;
      $vars['theme_key'] = $theme_key;

      // Force the widescreen view
      // Not sure if this should be retained. On emspace.com.au the main site switches between
      // wide and narrow, but the admin variation stays wide. This committed version is
      // essentially only the admin version, but we'll keep this here for now.
      
      if ($theme_key == 'emspace_2007' || arg(0) == 'gallery' || arg(0) == 'admin' || arg(0) == 'node' && (arg(1) == 'add' || arg(2) == 'edit')) {
        $body_classes[] = 'widescreen';
        $vars['pagetype'] = 'widescreen';
      }
      else {
        $vars['pagetype'] = (isset($vars['node']->field_page_layout)) ? $vars['node']->field_page_layout[0]['value'] : 'normal';
        $body_classes[] = $vars['pagetype'];
      }

      // Add a class to the theme if simplemenu is used (help to debug positional issues).
      if (user_access('view simplemenu')) {
        $body_classes[] = 'has_simplemenu';
      }

      // Print to far right if decided on the node
      if (arg(0) == 'node' && $vars['node']->field_far_right) {
        foreach ($vars['node']->field_far_right AS $term) {
          if($term['value']) {
            $vars['far_right_override'] = TRUE;
            $vars[$term['value']] = _emspace_far_right($term['value']);
          }
        }
      }
      // implode with spaces
      $vars['body_classes'] = implode(' ', $body_classes);

      if (drupal_get_title()) {
        $vars['head_title'] = strip_tags(drupal_get_title()) .' {emspace}';
      }
      else {
        $vars['head_title'] = '{emspace}';
      }

      $images = array('coffee.jpg', 'girly.jpg', 'glassies.jpg', 'pencil.jpg', 'phone.jpg');
      $img = $images[rand(0, count($images) - 1)];

      break;

    case 'node':

      // get the currently logged in user
      global $user;

      // set a new $is_admin variable
      // this is determined by looking at the currently logged in user and seeing if they are in the role 'admin'
      $vars['is_admin'] = in_array('admin', $user->roles);

      $node_classes = array('node');
      if ($vars['sticky']) {
      	$node_classes[] = 'sticky';
      }
      if (!$vars['node']->status) {
      	$node_classes[] = 'node-unpublished';
      }

      if ($user->uid == 1) {
//        dpr($vars['node']); die();
      }

      break;

    case 'comment':
      break;
  }
  return $vars;
}



