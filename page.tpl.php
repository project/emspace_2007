<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>

  <?php print $script_action; ?>

</head>
<body class="<?php print $body_classes; ?>">

<?php if ($theme_key == 'emspace'): ?>
  <div class="emspace_logo_header">
    <p>Em Space Design</p>
  </div>
  <img class="alt_img" id="alt_img" src="<?php print base_path();?>sites/all/themes/emspace/images/alt.gif" alt="Em Space Web Design"/>
<?php endif; ?>

<div class="page_main">
  <div class="page_top">
    <div class="header_left">
      <div class="header_right">
        <div class="menu_space">
          <?php if ($ironwork != ''): ?>
            <div id="ironwork" class="tabs">
              <?php print $ironwork; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  <!-- Insert header links etc.. here -->
  </div>

  <div class="column_left">
    <div id="column_left_inner">
      <div class="layout">
        <?php if ($secondary_links): ?>
          <div id="secondary" class="clear-block">
            <?php print theme('menu_links', $secondary_links); ?>
          </div>
        <?php endif; ?>
        <?php if ($sidebar_left): ?>
          <?php print $sidebar_left; ?>
      	<?php endif; ?>
      </div>
    </div>
  </div>
  <div class="column_center">
    <div id="column_center_inner">
      <?php print $header; ?>
      <?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
      <?php if ($content_top):?><div id="content-top"><?php print $content_top; ?></div><?php endif; ?>
      <?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
      <?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
      <?php print $help; ?>
      <?php print $messages; ?>
      <?php print $content; ?>
      <?php print $feed_icons; ?>
      <?php if ($content_bottom): ?><div id="content-bottom"><?php print $content_bottom; ?></div><?php endif; ?>
    </div>
  </div>
  <div class="column_right">
    <div id="column_right_inner">
      <?php if ($sidebar_right): ?>
        <div id="sidebar-right" class="column sidebar">
          <?php print $sidebar_right; ?>
        </div>
      <?php endif; ?>
    </div>
    <a id="action-menu" href="#"></a>
  </div>
  <div class="column_far_right">
    <div id="column_far_right_inner">
      <?php if ($far_right_override): ?>
        <?php print $donate; ?>
        <?php print $adsense; ?>
      <?php elseif ($sidebar_far_right): ?>
        <div id="sidebar-far-right" class="column sidebar">
          <?php print $sidebar_far_right; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="footer">

<!--
    <div class="desktop_image_wrap">
      <div class="desktop_image">
      </div>
    </div>
-->

      <?php print $footer_message; ?>
  </div>
</div>
<!-- absolute Divs -->


<?php print $desktop_image; ?>

<?php print $script_google; ?>

</body>
</html>